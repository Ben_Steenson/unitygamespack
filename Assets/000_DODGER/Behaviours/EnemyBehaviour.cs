﻿using UnityEngine;

namespace Dodger
{
	public class EnemyBehaviour : MonoBehaviour
	{
		/*	PUBLIC EVENTS	*/
		public delegate void OnDamageAction();
		public static event OnDamageAction OnDamage;
	}
}