﻿using UnityEngine;

namespace Dodger
{
	public class PlayerBehaviour : MonoBehaviour
	{
		/*	PUBLIC EVENTS	*/
		public delegate void PlayerHitAction();
		public static event PlayerHitAction OnPlayerHit;
	}
}