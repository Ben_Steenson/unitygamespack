﻿using UnityEngine;

namespace Dodger
{
	public class BulletBehaviour : MonoBehaviour
	{
		/*	PUBLIC EVENTS	*/
		public delegate void OnContactAction();
		public static event OnContactAction OnContact;
	}
}