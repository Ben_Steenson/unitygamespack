﻿using UnityEngine;
using UnityEngine.UI;

namespace Dodger
{
	public class GameManager : MonoBehaviour
	{
		[Header("User Interface")]
		public Text scoreBox = null;
		public Text livesBox = null;
		[Space(20)]

		[Header("Game Data")]
		[Range(1, 100)]
		public int maxLives = 1;
		private int currLives = 0;
	}
}